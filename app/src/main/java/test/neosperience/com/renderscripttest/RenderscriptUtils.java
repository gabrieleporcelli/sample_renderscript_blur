package test.neosperience.com.renderscripttest;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlend;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * Created by Gabriele Porcelli on 23/07/14.
 */
public class RenderscriptUtils {

    public static Bitmap blurImage(Context context, Bitmap bitmap, float radius) {
        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Bitmap blurredImage = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Allocation tmpIn = Allocation.createFromBitmap(rs, bitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, blurredImage);
        theIntrinsicBlur.setRadius(radius);
        theIntrinsicBlur.setInput(tmpIn);
        theIntrinsicBlur.forEach(tmpOut);
        tmpOut.copyTo(blurredImage);
        return blurredImage;
    }

    public static Bitmap blendImage(Context context, Bitmap bitmap1, Bitmap bitmap2) {
        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlend theIntrinsicBlend = ScriptIntrinsicBlend.create(rs, Element.U8_4(rs));
        Bitmap blurredImage = Bitmap.createBitmap(bitmap2.getWidth(),bitmap2.getHeight(), Bitmap.Config.ARGB_8888);
        Allocation tmpIn1 = Allocation.createFromBitmap(rs, bitmap1);
        Allocation tmpIn2 = Allocation.createFromBitmap(rs, bitmap2);
        theIntrinsicBlend.forEachSubtract(tmpIn1, tmpIn2);
        tmpIn2.copyTo(blurredImage);
        return blurredImage;
    }
}

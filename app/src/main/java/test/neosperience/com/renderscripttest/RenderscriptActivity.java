package test.neosperience.com.renderscripttest;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import static test.neosperience.com.renderscripttest.RenderscriptUtils.blurImage;

public class RenderscriptActivity extends Activity {

    boolean isBlurred = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renderscript);

        final ImageView imageView = (ImageView) findViewById(R.id.image);
        final ImageView blurredimageView = (ImageView) findViewById(R.id.image_blurred);

        //get image
        final Bitmap InputBitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.forest);

        //blur image
        final Bitmap bitmap = blurImage(this,InputBitmap1,25.f);
        //final Bitmap bitmap = blendImage(InputBitmap1, InputBitmap2);

        //set blurred image
        blurredimageView.setImageBitmap(bitmap);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBlurred) {
                    imageView.animate().setDuration(2000).alpha(1).start();
                    isBlurred = false;
                } else {
                    imageView.animate().setDuration(2000).alpha(0).start();
                    isBlurred = true;
                }
            }
        });
    }

    @Override
    //set immersive mode
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            final View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
